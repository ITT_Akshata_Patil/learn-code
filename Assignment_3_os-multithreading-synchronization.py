import threading 
balance = 100
T_Lock=threading.Lock()
def deposit(): 
    global balance
    for i in range(1000000):
        T_Lock.acquire()
        balance = balance + 1
        T_Lock.release()
        
def withdraw(): 
    global balance
    for i in range(1000000):
        T_Lock.acquire()
        balance = balance - 1
        T_Lock.release()

if __name__ == "__main__": 
    balance = int(input().strip())

    t1 = threading.Thread(target=deposit, args=()) 
    t2 = threading.Thread(target=withdraw, args=()) 
  
    t1.start() 
    t2.start() 
  
    t1.join() 
    t2.join() 
    
    print("all done: balance = " + str(balance))