import threading
import time
import math
import os
import random
import re
import sys

count = 0
def add():
  global count
  for x in range(100000):
       count = count + 1;
  time.sleep(1)

if __name__ == '__main__':
    input = int(input().strip())
    thread_arr = []
    for i in range(int(input/100000)):
        t = threading.Thread(target=add)
        t.start()
        thread_arr.append(t)
        time.sleep(0.2)
             
    for t in thread_arr:
        t.join()

print(count)

