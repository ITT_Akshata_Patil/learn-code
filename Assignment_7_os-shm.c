#define _XOPEN_SOURCE 600
#define _POSIX_C_SOURCE 200112L
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

void child();
void parent();  
void process_stage(int stage);
int shmid;
int stages=0;

int create_shared_mem(int size)
{   
    int shmid;
    key_t key = 'a';
    shmid = shmget(key, size, 0666|IPC_CREAT);  
 	return shmid;
}

void write_message(int shmid, char * message)
{
	char *str = (char*) shmat(shmid, (void*)0,0);
    snprintf(str, strlen(message)+1, "%s\n", message);
}

char *read_message(int shmid, int length)
{
    char *read_msg = malloc(sizeof(char)*length);
    char *str = (char*) shmat(shmid, (void*)0,0);
    strncpy(read_msg, str, length);
	return read_msg;
}

void remove_shared_mem(int shmid)
{
    shmctl(shmid, IPC_RMID, NULL);
}

int get_child_exit_status()
{
        int stat;
        wait(&stat);
        return WEXITSTATUS(stat);
}

void child(){		
	for(int i=1;i<=stages;i++)	
	{
		char message[32];
		sprintf(message,"%s%d","STAGE",i);
		process_stage(i);
 		
		write_message(shmid,message);
		usleep(10000);
	}
        exit(stages); 
}

void parent()
{
	char *last_message=NULL;
	char *message=NULL; 

	for(int i=1;i<=stages;i++)	
	{
		printf("Waiting for the child to finish the stage:%d\n",i);
		fflush(stdout);
		do
		{
			usleep(1000);
			last_message=message;
			message=read_message(shmid,32);	

		}
        while((last_message==NULL && message == NULL) 
		|| (last_message!=NULL && strcmp(last_message,message)==0));
		printf("STAGE completed:%s\n",message);
		fflush(stdout);
	}
	printf("Child exited with status:%d\n",get_child_exit_status());
	remove_shared_mem(shmid);
}

void process_stage(int stage)
{	
	printf("Procesing stage%d\n",stage);
	usleep(1);
	fflush(stdout);
}

int main(int argc, char* argv[]) 
{ 
	pid_t cid; 
	shmid = create_shared_mem(100); 
	if(shmid == 0 )
	{
		printf("Shared Mem creation failed\n");
	}
	scanf("%d",&stages);
	cid = fork(); 
    	
	if (cid == 0) 
	{ 
		child();
	} else if(cid > 0 )
	{
		parent();
	}
} 
