#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#define MAX_SIZE 100

struct Stack_t
{
    unsigned int expression_length;
    int top;
    int array[MAX_SIZE];
};

struct Stack_t* stackCreation(unsigned int length){
    struct Stack_t* stack = (struct Stack_t *)malloc(sizeof(struct Stack_t));
    if(stack == NULL)
        return NULL;
    stack->top = -1;
    stack->expression_length = length;
    return stack;
}

int isStackOverFlow(struct Stack_t *stack){
    return stack->top == MAX_SIZE-1;
}

void pushItemToStack(struct Stack_t *stack, int operand){
    if(!isStackOverFlow(stack))
        stack->array[++stack->top] = operand;
    else
        return;
}

int isStackUnderFlow(struct Stack_t *stack){
    return stack->top == -1;
}

int popItemFromStack(struct Stack_t *stack){
    if(!isStackUnderFlow(stack))
        return stack->array[stack->top--];
    else 
        return -1;
}

int calculate(int operand1, int operand2, char operator)
{
    int result = 0;
     switch(operator)
     {
        case '+':
            result = operand2 + operand1;
            break;
        case '-':
            result = operand2 - operand1;
            break;
        case '*':
            result = operand2 * operand1;
            break;
        case '/':
            result = operand2 / operand1;
            break;
    }  
    return result;
}

int evaluatePostfixExpression(char* postfixExp){
    struct Stack_t *stack = stackCreation(strlen(postfixExp));
    char character;
    int operand1, operand2,result=0;
   
    if(stack == NULL)
        return -1;
    
    for(int iter = 0; postfixExp[iter]; ++iter ){
        character = postfixExp[iter];
        if(character == ' ')
            continue;
        else if(isdigit(character)){
            int num = 0;
            while(isdigit(postfixExp[iter]))
            {
                num = num * 10 + (postfixExp[iter] - '0');
                iter++;
            }
            iter--;
            pushItemToStack(stack,num);
        }
        else
        {
            operand1 = popItemFromStack(stack);
            operand2 = popItemFromStack(stack);
            if(operand1 == -1 || operand2 == -1)
                return 0; 
            result = calculate(operand1, operand2, postfixExp[iter]);
           
            pushItemToStack(stack, result);
        }
        
    }
     return popItemFromStack(stack);
}

int main() {
    char postfixExp[MAX_SIZE];
    scanf("%[^\n]%*c", postfixExp);
    if(strlen(postfixExp) > 100)
        return 0;
    printf("%d",evaluatePostfixExpression(postfixExp));

    return 0;
}