#include <stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include<sys/wait.h>

pid_t ppid;
pid_t child_pid;

void child();
void parent();

int get_child_exit_status()
{   
        int result, result_val = 0;       
        wait(&result);
        if (WIFEXITED(result)){
            result_val = WEXITSTATUS(result);
        }
        if(result_val > 1 && result_val <= 128)
        {
            return result_val;
        }
        else
        {
            printf("Please enter the correct input");
            exit(0);
        }
}
int main(void)
{         
    ppid = getpid();
    child_pid = fork();

    if(child_pid == 0)
    {  
        child(); 
    }
    else if(child_pid> 0)
    {
        printf("Child exited with status=%d",get_child_exit_status());
    }
}
void child()
{
        pid_t c_pid = getpid();
        if(c_pid == ppid)
        {
                printf("This is not a child\n");
                return;
        }
        printf("This is a child\n");
        int status;
        scanf("%d",&status);
        exit(status);
}