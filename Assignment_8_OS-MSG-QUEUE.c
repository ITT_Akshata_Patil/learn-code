#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include<sys/msg.h>
void child();
void parent();  
int get_child_exit_status();
int msgid;
int commands=0;

typedef struct command_type
{
	long type;
	char command[32];
	int args[2];
} command_t;
typedef struct result_type 
{
	long type;
	int result;
} result_t;

int create_message_queue()
{
        key_t key; 
        key = ftok("progfile", 65); 
        msgid = msgget(key, 0666 | IPC_CREAT); 
        if (msgid < 0)
        { 
            printf("failed to create message id "); 
            exit(0); 
        } 
        return msgid;    
}

int send_command(int msgid, command_t cmd)
{
        cmd.type=1; 
        int message_id; 
        message_id=msgsnd(msgid, &cmd, sizeof(command_t), 0); 
        if (message_id < 0)
        { 
            printf("failed to send command"); 
            exit(0); 
        } 
        return message_id;  
}

command_t *recv_command(int msgid)
{
	int rcv_status; 
    command_t *read_command = malloc(sizeof(*read_command)); 
    rcv_status = msgrcv(msgid, read_command, sizeof(*read_command), 1, 0); 
    if (rcv_status < 0)
    { 
        printf("failed to read command"); 
        exit(0); 
    } 
    return read_command;
}

int send_result(int msgid, result_t result)
{
	int message_id; 
    result.type=3; 
    message_id=msgsnd(msgid, &result, sizeof(result_t), 0); 
    if (message_id < 0)
    { 
        printf("failed to send result"); 
        exit(0); 
    } 
    return message_id;
}

result_t *recv_result(int msgid)
{
	int rcv_status; 
    result_t *read_result = malloc(sizeof(*read_result)); 
    rcv_status=msgrcv(msgid, read_result, sizeof(*read_result), 3, 0); 
    if (rcv_status < 0)
    { 
        printf("failed to read result"); 
        exit(0); 
    } 
    return read_result;
}

void delete_message_queue(int msgid)
{
    msgctl(msgid, IPC_RMID, NULL);	
}

void parent()
{ 
    for(int i=1;i<=commands;i++) 
    { 
        command_t cmd; 
        result_t *result; 
        scanf("%s",cmd.command); 
        scanf("%d",&cmd.args[0]); 
        scanf("%d",&cmd.args[1]); 
        send_command(msgid,cmd); 
        
        result=recv_result(msgid); 
        printf("CMD=%s, RES = %d\n",cmd.command,result->result); 
    } 
    printf("Child exited with status:%d\n",get_child_exit_status()); 
    delete_message_queue(msgid); 
} 

void child()
{
	for(int i=1;i<=commands;i++)	
	{
		command_t *cmd;
        cmd=recv_command(msgid); 
        result_t result; 
        if (strcmp(cmd->command,"ADD")==0)
        { 
            result.result=cmd->args[0]+cmd->args[1]; 
        } 
        if (strcmp(cmd->command,"MUL")==0)
        { 
            result.result=cmd->args[0]*cmd->args[1]; 
        } 
        if (strcmp(cmd->command,"SUB")==0)
        { 
            result.result=cmd->args[0]-cmd->args[1]; 
        } 
        send_result(msgid,result); 
	}
        exit(commands); 
}

int main(int argc, char* argv[]) 
{ 
	pid_t cid; 
	msgid = create_message_queue(); 
	scanf("%d",&commands);
	if(msgid <= 0 )
	{
		printf("Message Queue creation failed\n");
	}
	cid = fork(); 

	if (cid == 0) 
	{ 
		child();
	} else if(cid > 0 )
	{
		parent();
	}
} 

int get_child_exit_status()
{
        int stat;
        wait(&stat);
        return WEXITSTATUS(stat);
}